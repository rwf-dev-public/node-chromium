FROM node:16.13.1-buster
# installing chromium is necessaray, else some shared librarie are missing that puppeteer relies on
RUN apt-get update \
 && apt-get install -y chromium qpdf
